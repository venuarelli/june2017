package com.utils;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

public class AmazonPropertyUtil {

    Properties properties = new Properties();

    public AmazonPropertyUtil() {
        FileReader reader;
        try {
            reader = new FileReader(new File("src/main/resources/default.properties"));
            properties.load(reader);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String getProperty(String property) {
        return properties.getProperty(property);
    }
}
