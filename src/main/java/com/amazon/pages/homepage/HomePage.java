package com.amazon.pages.homepage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amazon.pages.BasePage;

public class HomePage extends BasePage {

    @FindBy(id = "nav-search-submit-text")
    WebElement searchButton;

    @FindBy(name = "field-keywords")
    WebElement searchField;

    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void clickOnSearchButton() {
        searchButton.click();
    }

    public void clickOnSearchField() {
        searchField.click();
    }

}
