package com.amazon.pages.homepage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amazon.pages.BasePage;

public class AmazonHomePage extends BasePage {

    @FindBy(xpath = "//span[@class='nav-action-inner']")
    WebElement signinButton;

    @FindBy(xpath = "//span[text()='Hello. Sign in']")
    WebElement signinHover;

    public AmazonHomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void clickOnSigninButton() {
        signinButton.click();

    }

    public void moveToSigninHover() {
        findVisibilityOfElement(signinHover);
        moveToElement(signinHover);
    }
}
