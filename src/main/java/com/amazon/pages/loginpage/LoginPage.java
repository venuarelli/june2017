package com.amazon.pages.loginpage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amazon.pages.BasePage;
import com.amazon.pages.homepage.HomePage;

public class LoginPage extends BasePage {

    @FindBy(id = "ap_email")
    WebElement email;

    @FindBy(id = "ap_password")
    WebElement password;

    @FindBy(id = "signInSubmit")
    WebElement signinButton;

    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void amazonLogin() {
        String email = propertyUtil.getProperty("amazon_username");
        String password = propertyUtil.getProperty("amazon_password");
        amazonLogin(email, password);
    }

    public void amazonLogin(String email, String password) {
        setUserName(email);
        setPassword(password);
        // clickOnSigninButton();
    }

    public HomePage clickOnSigninButton() {
        signinButton.click();
        return new HomePage(driver);
    }

    public void setPassword(String password) {
        clearAndType(this.password, password);
    }

    public void setUserName(String email) {
        clearAndType(this.email, email);
    }
}
