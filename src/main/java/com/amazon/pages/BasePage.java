package com.amazon.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.utils.AmazonPropertyUtil;

public class BasePage {
    public WebDriver driver;

    private int MAXIMUM_TIMEOUT = 30;

    public AmazonPropertyUtil propertyUtil;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        propertyUtil = new AmazonPropertyUtil();
    }

    public void clearAndType(WebElement element, String text) {
        element.clear();
        element.sendKeys(text);
    }

    public void findVisibilityOfElement(By by) {
        WebDriverWait wait = new WebDriverWait(driver, MAXIMUM_TIMEOUT);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void findVisibilityOfElement(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, MAXIMUM_TIMEOUT);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void moveToElement(WebElement element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(element).build().perform();
    }

}
