package com.programs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DataDrivenExample {
    WebDriver driver;

    @AfterClass
    public void closeBrowser() {
        driver.close();
    }

    @DataProvider(name = "loginDetails")
    public Object[][] getData() {
        Object[][] obj = { {"venuarelli", "12345678"}, {"aarellivenu", "12345678"}};
        return obj;
    }

    @Test(dataProvider = "loginDetails")
    public void loginToGmail(String name, String pass) {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://accounts.google.com");

    }
}
