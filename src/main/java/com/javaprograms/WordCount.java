package com.javaprograms;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class WordCount {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter string value");
        String str = sc.nextLine();
        // char[] str1 = str.split(" ");
        HashMap<Character, Integer> hm = new HashMap<Character, Integer>();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (hm.containsKey(c)) {
                hm.put(c, hm.get(c) + 1);
            } else {
                hm.put(c, 1);
            }

        }
        for (Entry<Character, Integer> me : hm.entrySet()) {
            if (me.getValue() == 1) {
                System.out.println(me.getKey() + " : " + me.getValue());
            }
        }
    }

}
