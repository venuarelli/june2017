package com.javaprograms;

import java.util.Scanner;

public class FindAreaOfCircle {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Radius :");
        /* double pi = 3.14; */
        double area;
        int radius = sc.nextInt();
        area = Math.PI * (radius * radius);
        System.out.println(area);

    }
}
