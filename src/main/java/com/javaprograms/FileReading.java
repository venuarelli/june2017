package com.javaprograms;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileReading {
    public static void main(String[] args) throws Exception {

        File file = new File("F:\\Selenium data\\Sample.txt");
        System.out.println("File exist" + file.exists());
        System.out.println("File exist" + file.getAbsolutePath());
        try {
            System.out.println("File exist" + file.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("File exist" + file.getName());
        System.out.println("File exist" + file.getPath());

        FileReader fileReader = new FileReader(file);

        BufferedReader bufferReader = new BufferedReader(fileReader);

        String str = bufferReader.readLine();
        System.out.println(str);
        while (str != null) {
            System.out.println(str);
            str = bufferReader.readLine();
        }
        bufferReader.close();
        fileReader.close();
    }

}
