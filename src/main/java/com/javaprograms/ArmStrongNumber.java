package com.javaprograms;

import java.util.Scanner;

public class ArmStrongNumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter any number");
        int num = sc.nextInt();
        int temp = num;
        int r = 0, x;
        while (num > 0) {
            x = num % 10;
            num = num / 10;
            r = r + (x * x * x);
        }
        if (temp == r) {
            System.out.println("Given number is Armstrong");
        } else {
            System.out.println("not");
        }
    }
}
