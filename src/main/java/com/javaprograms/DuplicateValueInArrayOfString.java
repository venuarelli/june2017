package com.javaprograms;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class DuplicateValueInArrayOfString {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter String Value  : ");
        String str = sc.nextLine();
        String[] arr = str.split(",");
        // String[] str = {"Java", "Net", "Java", "C", "C"};
        Set<String> set = new HashSet<String>();
        for (String name : arr) {
            if (!set.add(name)) {
                System.out.println("Duplicate Word : " + name);
            }
        }
    }
}