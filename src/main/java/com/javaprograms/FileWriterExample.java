package com.javaprograms;

import java.io.FileWriter;
import java.io.IOException;

public class FileWriterExample {
    public static void main(String[] args) {
        FileWriter fw = null;
        try {
            fw = new FileWriter("F:\\Selenium data\\Sample.txt");
            fw.write("Sample file created2");
            fw.write("");
            fw.write("\r\n" + "The file is not created.");
            fw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
