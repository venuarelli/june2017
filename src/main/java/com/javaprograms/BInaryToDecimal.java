package com.javaprograms;

import java.util.Scanner;

public class BInaryToDecimal {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Binary value.");
        int x = 0, y = 0;
        int n = sc.nextInt();

        while (n != 0) {
            x += ((n % 10) * Math.pow(2, y));
            n = n / 10;
            y++;
        }
        System.out.println(x);
    }
}
