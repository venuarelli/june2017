package com.javaprograms;

import java.util.Scanner;

public class FirstNonRepeatedCharInString {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter String :");
        String str = sc.nextLine();
        // String str = "welcome";
        int count = 0;
        boolean flag;
        char[] ch = str.toCharArray();

        for (int i = 0; i < ch.length; i++) {
            flag = true;
            for (int j = 0; j < i; j++) {
                if (ch[i] == str.charAt(j)) {
                    flag = false;
                }
            }
            if (flag) {
                for (int k = 0; k < str.length(); k++) {
                    if (ch[i] == str.charAt(k)) {
                        count++;
                    }
                }
                if (count == 1) {
                    System.out.println("Non-Repeated First Char :" + ch[i]);
                    break;
                }
                count = 0;
            }
        }
    }
}
