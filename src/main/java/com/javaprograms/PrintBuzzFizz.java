package com.javaprograms;

import java.util.Scanner;

public class PrintBuzzFizz {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number");
        int num = sc.nextInt();
        for (int i = 1; i <= num; i++) {
            if ((i % (3 * 5)) == 0) {
                System.out.print("BuzzFizz \n");
            } else if ((i % 5) == 0) {
                System.out.print("Fizz \n");
            } else if (i % 3 == 0) {
                System.out.print("Buzz \n");
            } else {
                System.out.println(i);
            }

        }
    }
}
