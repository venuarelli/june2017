package com.javaprograms;

import java.util.Scanner;

public class FirstRepeatingEleInArray {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Size of an Array :");
        int size = sc.nextInt();
        int count = 0;
        boolean flag = false;
        int arr[] = new int[size];
        System.out.println("Enter Array values :");
        for (int i = 0; i < size; i++) {
            arr[i] = sc.nextInt();
        }

        for (int a = 0; a < arr.length; a++) {
            flag = true;
            for (int b = 0; b < a; b++) {
                if (arr[a] == arr[b]) {
                    flag = false;
                }
            }

            if (flag) {
                for (int element : arr) {
                    if (arr[a] == element) {
                        count++;
                    }
                }
                if (count > 1) {
                    System.out.println(arr[a]);
                    break;
                }
                count = 0;
            }

        }
    }
}
