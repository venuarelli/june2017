package com.javaprograms;

public class DuplicateElementsInString {

    public static void main(String[] args) {
        String str = "venu vani venky";
        char[] ch = str.toCharArray();
        int count = 0;
        boolean flag;
        for (int i = 0; i < ch.length; i++) {
            flag = true;
            for (int j = 0; j < i; j++) {
                if (ch[i] == str.charAt(j)) {
                    flag = false;
                }
            }

            if (flag) {
                for (int k = 0; k < str.length(); k++) {
                    if (ch[i] == str.charAt(k)) {
                        count++;
                    }
                }
                if (count > 1) {
                    if (ch[i] != ' ') {
                        System.out.println(ch[i] + "--" + count);
                    }
                }
                count = 0;
            }

        }

    }
}
