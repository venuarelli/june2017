package com.javaprograms;

public class DuplicateElementsInArray {

    public static void main(String[] args) {
        /*
         * Scanner sc = new Scanner(System.in); System.out.println("Enter size of an Array"); int
         * size = sc.nextInt(); int a[] = new int[size]; System.out.println("Enter Array Elements");
         * for (int i = 0; i < size; i++) { a[i] = sc.nextInt(); } int count = 0; boolean flag; for
         * (int i = 0; i < a.length; i++) { flag = true; for (int j = 0; j < i; j++) { if (a[i] ==
         * a[j]) { flag = false; } } if (flag) { for (int element : a) { if (a[i] == element) {
         * count++; } } if (count == 1) { // System.out.println(" Duplicate elements are :" + a[i] +
         * "--" + count); System.out.println(a[i]); } count = 0; } }
         */

        int a[] = {1, 2, 3, 4, 3, 5};
        int count = 0;
        boolean flag;

        for (int i = 0; i < a.length; i++) {
            flag = true;
            for (int j = 0; j < i; j++) {
                if (a[i] == a[j]) {
                    flag = false;
                }
            }
            if (flag) {
                for (int element : a) {
                    if (a[i] == element) {
                        count++;
                    }
                }
                if (count > 1) {
                    System.out.println(a[i]);
                }
                count = 0;
            }
        }

    }
}
