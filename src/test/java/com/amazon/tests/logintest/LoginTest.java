package com.amazon.tests.logintest;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.amazon.pages.homepage.AmazonHomePage;
import com.amazon.pages.loginpage.LoginPage;
import com.amazon.tests.BaseLoginToAmazon;

public class LoginTest extends BaseLoginToAmazon {

    AmazonHomePage amazonHomePage;
    LoginPage loginPage;

    @BeforeClass
    public void loginPage() {
        loginPage = new LoginPage(webDriver);
        amazonHomePage = new AmazonHomePage(webDriver);
        amazonHomePage.moveToSigninHover();
        amazonHomePage.clickOnSigninButton();
    }

    @Test
    public void testLogin() {
        loginPage.amazonLogin();

    }
}
