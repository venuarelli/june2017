package com.amazon.tests;

import org.testng.annotations.BeforeTest;

import com.utils.AmazonPropertyUtil;

public class BaseLoginToAmazon extends BaseTest {

    AmazonPropertyUtil propertyUtil;

    // @AfterTest
    public void closeBrowser() {
        webDriver.close();
    }

    @BeforeTest
    public void openBrowser() {
        webDriver = getWebDriver();
        webDriver.get(getHomePageUrl());
        webDriver.manage().window().maximize();
    }

}
